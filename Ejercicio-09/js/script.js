var texto = document.getElementById("texto");

function letraCapital() {
    document.getElementById("capitalize").addEventListener("click", function () {
        texto.style.textTransform = "capitalize";
    })
}
letraCapital();

function letraMayuscula() {
    document.getElementById("upperCase").addEventListener("click", function () {
        texto.style.textTransform = "upperCase";
    })
}
letraMayuscula();

function letraMinuscula() {
    document.getElementById("lowerCase").addEventListener("click", function () {
        texto.style.textTransform = "lowerCase";
    })
}
letraMinuscula();

function letraOriginal() {
    document.getElementById("original").addEventListener("click", function () {
        texto.style.textTransform = "";
    })
}
letraOriginal();