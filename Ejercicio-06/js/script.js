var foto = document.getElementById("foto");

foto.addEventListener("mouseover", function () {
    foto.style.filter = "grayscale(100%)";
    foto.style.transitionDuration = "1s";
})
foto.addEventListener("mouseout", function () {
    foto.style.filter = "";
    foto.style.transitionDuration = "1s";
})