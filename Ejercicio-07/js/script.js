var foto = document.getElementById("foto");

foto.addEventListener("mouseover", function () {
    foto.style.border = "5px solid black";
    foto.style.boxShadow = "5px 5px 5px rgba( 0, 0, 0, 0.5)"
    foto.style.transitionDuration = "1s";
})
foto.addEventListener("mouseout", function () {
    foto.style.border = "0px solid black";
    foto.style.boxShadow = "5px 5px 5px rgba( 0, 0, 0, 0.5)"
    foto.style.transitionDuration = "1s";
})